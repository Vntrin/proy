﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Librado.Enums
{
    public enum Empaque {C20, C24, Pieza}
    public class EEmpaque
    {
        public static string getString(Empaque empaque)
        {
            switch(empaque)
            {
                case Empaque.C20:
                    return "C 20";
                case Empaque.C24:
                    return "C 24";
                case Empaque.Pieza:
                    return "Pieza";
                default:
                    return "NA";
            }
        }
    }
}
