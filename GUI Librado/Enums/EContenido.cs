﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Librado.Enums
{
    public enum Contenido {ml330, ml355, ml500, L5}
    public class EContenido
    {
        public static string getString(Contenido cont)
        {
            switch (cont)
            {
                case Contenido.ml330:
                    return "300 ml";
                case Contenido.ml355:
                    return "355 ml";
                case Contenido.ml500:
                    return "500 ml";
                case Contenido.L5:
                    return "5 L";
                default:
                    return "NA";

            }
        }
    }
}
