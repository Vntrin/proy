﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Classes;

namespace GUI_Librado
{
    public partial class Pedidos : Form
    {
        static ArchivosPedidos archivosPedido = new ArchivosPedidos();
        static InventarioC inventario = new InventarioC();
        public Pedidos(ArchivosPedidos archivosPedidos, InventarioC inventarios)
        {
            InitializeComponent();
            archivosPedido = archivosPedidos;
            inventario = inventarios;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            AñadirReabastecimiento añadirReabastecimiento = new AñadirReabastecimiento(archivosPedido, inventario);
            añadirReabastecimiento.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AñadirPedido añadirPedido = new AñadirPedido(archivosPedido, inventario);
            añadirPedido.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Pedidos_Load(object sender, EventArgs e)
        {
            foreach(PedidoCompra pedido in archivosPedido.Compras)
            {
                LstVw.Items.Add(pedido.Fecha.ToString());
                LstVwDTE.Items.Add("NA");
                LstViwPrc.Items.Add(pedido.Precio.ToString());
                LstVwPrdct.Items.Add(pedido.Cervezas.CodigoSerial);
                LstVwStd.Items.Add("NA");
                LstVwTp.Items.Add("Compra");
            }
            foreach (PedidoVenta pedido in archivosPedido.Ventas)
            {
                LstVw.Items.Add(pedido.Fecha.ToString());
                LstVwDTE.Items.Add(pedido.FechaEntrega.ToString());
                LstViwPrc.Items.Add(pedido.Precio.ToString());
                LstVwPrdct.Items.Add(pedido.Cervezas.CodigoSerial);
                LstVwTp.Items.Add("Venta");
                if (pedido.Estado)
                    LstVwStd.Items.Add("Ya se completo");
                else
                    LstVwStd.Items.Add("En proceso");
            }
        }
    }
}
