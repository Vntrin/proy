﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Forms;
using GUI_Librado.Classes;


namespace GUI_Librado
{
    public partial class MainMenu : Form
    {
        static InventarioC inventarios = new InventarioC();
        static ArchivosPedidos archivosPedidos = new ArchivosPedidos();

        public MainMenu()
        {
            InitializeComponent();
        }

        private void MainMenu_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Inventario inventario = new Inventario(inventarios);
            inventario.ShowDialog();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Pedidos pedidos = new Pedidos();
            //pedidos.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Impresión impresion = new Impresión();
         
            impresion.ShowDialog();
   
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Códigos códigos = new Códigos(inventarios);
          
            códigos.Show();
  
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            Impresión impresion = new Impresión();
      
            impresion.ShowDialog();
    
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            Pedidos pedidos = new Pedidos(archivosPedidos,inventarios);
            pedidos.ShowDialog();

        }

        private void button5_Click_1(object sender, EventArgs e)
        {
            Códigos códigos = new Códigos(inventarios);

            códigos.Show();
       
        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
