﻿namespace GUI_Librado
{
    partial class Inventario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LstVwMrc = new System.Windows.Forms.ListView();
            this.LstVwNmbr = new System.Windows.Forms.ListView();
            this.LstVwNcnldd = new System.Windows.Forms.ListView();
            this.LstVwDscpn = new System.Windows.Forms.ListView();
            this.LstVwCntnd = new System.Windows.Forms.ListView();
            this.LstVwMpq = new System.Windows.Forms.ListView();
            this.LstVwLchl = new System.Windows.Forms.ListView();
            this.LstVwPCIEPSIVA = new System.Windows.Forms.ListView();
            this.LstVwCD = new System.Windows.Forms.ListView();
            this.LstVwCdg = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.49831F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.208595F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.22395F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.24927F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.231743F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.621227F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.055501F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.302824F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.132303F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.0309F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label9, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.label8, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label10, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.LstVwMrc, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwNmbr, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwNcnldd, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwDscpn, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwCntnd, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwMpq, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwLchl, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwPCIEPSIVA, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwCD, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwCdg, 9, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 3);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(0, 10, 10, 58);
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 158F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(2081, 801);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Marca";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(350, 10);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nacionalidad";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(181, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(1618, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 150);
            this.label9.TabIndex = 2;
            this.label9.Text = "Cantidad Disponible (En Botellas/ Barriles/ Piezas)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(560, 10);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(125, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "Descripción";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(1468, 10);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 50);
            this.label7.TabIndex = 2;
            this.label7.Text = "Precio Caja\r\nIEPS e IVA";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1744, 10);
            this.label10.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 25);
            this.label10.TabIndex = 2;
            this.label10.Text = "Código";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1018, 10);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Contenido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1282, 10);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "° Alcohol";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1146, 10);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Empaque";
            // 
            // LstVwMrc
            // 
            this.LstVwMrc.Location = new System.Drawing.Point(3, 171);
            this.LstVwMrc.Name = "LstVwMrc";
            this.LstVwMrc.Size = new System.Drawing.Size(169, 569);
            this.LstVwMrc.TabIndex = 3;
            this.LstVwMrc.UseCompatibleStateImageBehavior = false;
            this.LstVwMrc.View = System.Windows.Forms.View.List;
            // 
            // LstVwNmbr
            // 
            this.LstVwNmbr.Location = new System.Drawing.Point(178, 171);
            this.LstVwNmbr.Name = "LstVwNmbr";
            this.LstVwNmbr.Size = new System.Drawing.Size(163, 569);
            this.LstVwNmbr.TabIndex = 4;
            this.LstVwNmbr.UseCompatibleStateImageBehavior = false;
            this.LstVwNmbr.View = System.Windows.Forms.View.List;
            // 
            // LstVwNcnldd
            // 
            this.LstVwNcnldd.Location = new System.Drawing.Point(347, 171);
            this.LstVwNcnldd.Name = "LstVwNcnldd";
            this.LstVwNcnldd.Size = new System.Drawing.Size(204, 569);
            this.LstVwNcnldd.TabIndex = 5;
            this.LstVwNcnldd.UseCompatibleStateImageBehavior = false;
            this.LstVwNcnldd.View = System.Windows.Forms.View.List;
            // 
            // LstVwDscpn
            // 
            this.LstVwDscpn.Location = new System.Drawing.Point(557, 171);
            this.LstVwDscpn.Name = "LstVwDscpn";
            this.LstVwDscpn.Size = new System.Drawing.Size(452, 569);
            this.LstVwDscpn.TabIndex = 6;
            this.LstVwDscpn.UseCompatibleStateImageBehavior = false;
            this.LstVwDscpn.View = System.Windows.Forms.View.List;
            // 
            // LstVwCntnd
            // 
            this.LstVwCntnd.Location = new System.Drawing.Point(1015, 171);
            this.LstVwCntnd.Name = "LstVwCntnd";
            this.LstVwCntnd.Size = new System.Drawing.Size(122, 569);
            this.LstVwCntnd.TabIndex = 7;
            this.LstVwCntnd.UseCompatibleStateImageBehavior = false;
            this.LstVwCntnd.View = System.Windows.Forms.View.List;
            // 
            // LstVwMpq
            // 
            this.LstVwMpq.Location = new System.Drawing.Point(1143, 171);
            this.LstVwMpq.Name = "LstVwMpq";
            this.LstVwMpq.Size = new System.Drawing.Size(130, 569);
            this.LstVwMpq.TabIndex = 8;
            this.LstVwMpq.UseCompatibleStateImageBehavior = false;
            this.LstVwMpq.View = System.Windows.Forms.View.List;
            // 
            // LstVwLchl
            // 
            this.LstVwLchl.Location = new System.Drawing.Point(1279, 171);
            this.LstVwLchl.Name = "LstVwLchl";
            this.LstVwLchl.Size = new System.Drawing.Size(180, 569);
            this.LstVwLchl.TabIndex = 9;
            this.LstVwLchl.UseCompatibleStateImageBehavior = false;
            this.LstVwLchl.View = System.Windows.Forms.View.List;
            // 
            // LstVwPCIEPSIVA
            // 
            this.LstVwPCIEPSIVA.Location = new System.Drawing.Point(1465, 171);
            this.LstVwPCIEPSIVA.Name = "LstVwPCIEPSIVA";
            this.LstVwPCIEPSIVA.Size = new System.Drawing.Size(142, 569);
            this.LstVwPCIEPSIVA.TabIndex = 10;
            this.LstVwPCIEPSIVA.UseCompatibleStateImageBehavior = false;
            this.LstVwPCIEPSIVA.View = System.Windows.Forms.View.List;
            // 
            // LstVwCD
            // 
            this.LstVwCD.Location = new System.Drawing.Point(1615, 171);
            this.LstVwCD.Name = "LstVwCD";
            this.LstVwCD.Size = new System.Drawing.Size(120, 569);
            this.LstVwCD.TabIndex = 11;
            this.LstVwCD.UseCompatibleStateImageBehavior = false;
            this.LstVwCD.View = System.Windows.Forms.View.List;
            // 
            // LstVwCdg
            // 
            this.LstVwCdg.Location = new System.Drawing.Point(1741, 171);
            this.LstVwCdg.Name = "LstVwCdg";
            this.LstVwCdg.Size = new System.Drawing.Size(327, 569);
            this.LstVwCdg.TabIndex = 12;
            this.LstVwCdg.UseCompatibleStateImageBehavior = false;
            this.LstVwCdg.View = System.Windows.Forms.View.List;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 6);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(348, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Añadir Nueva Cerveza";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1838, 835);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(200, 44);
            this.button2.TabIndex = 2;
            this.button2.Text = "Regresar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.button3);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(36, 829);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(768, 73);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(366, 6);
            this.button3.Margin = new System.Windows.Forms.Padding(6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(370, 44);
            this.button3.TabIndex = 3;
            this.button3.Text = "Modificar Cantidades";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(1613, 835);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(216, 44);
            this.button4.TabIndex = 4;
            this.button4.Text = "Actualizar";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Inventario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(2152, 917);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Inventario";
            this.Text = "Inventario";
            this.Load += new System.EventHandler(this.Inventario_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ListView LstVwMrc;
        private System.Windows.Forms.ListView LstVwNmbr;
        private System.Windows.Forms.ListView LstVwNcnldd;
        private System.Windows.Forms.ListView LstVwDscpn;
        private System.Windows.Forms.ListView LstVwCntnd;
        private System.Windows.Forms.ListView LstVwMpq;
        private System.Windows.Forms.ListView LstVwLchl;
        private System.Windows.Forms.ListView LstVwPCIEPSIVA;
        private System.Windows.Forms.ListView LstVwCD;
        private System.Windows.Forms.ListView LstVwCdg;
        private System.Windows.Forms.Button button4;
    }
}