﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Classes;
using GUI_Librado.Enums;
using GUI_Librado.Forms;

namespace GUI_Librado
{
    public partial class Inventario : Form
    {
        static InventarioC inventarios = new InventarioC();
        public Inventario(InventarioC inventario)
        {
            InitializeComponent();
            inventarios = inventario;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            //OnClosed(EventArgs e)
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AñadirCerveza añadirCerveza = new AñadirCerveza(inventarios);
            añadirCerveza.ShowDialog();
        }

        private void Inventario_Load(object sender, EventArgs e)
        {
            foreach (Cerveza cerveza in inventarios.Lista)
            {
                LstVwCD.Items.Add(cerveza.Cantidad.ToString());
                LstVwCdg.Items.Add(cerveza.CodigoSerial);
                LstVwCntnd.Items.Add(EContenido.getString(cerveza.CervezaCarac.Contenido));
                LstVwDscpn.Items.Add(cerveza.Descripcion);
                LstVwLchl.Items.Add(EGrado.getString(cerveza.CervezaCarac.Grado));
                LstVwMpq.Items.Add(EEmpaque.getString(cerveza.CervezaCarac.Empaque));
                LstVwMrc.Items.Add(ECerveceria.getString(cerveza.CervezaCarac.Cerveceria));
                LstVwNcnldd.Items.Add(EPais.getString(cerveza.CervezaCarac.Pais));
                LstVwNmbr.Items.Add(cerveza.Nombre);
                LstVwPCIEPSIVA.Items.Add(cerveza.PrecioVenta.ToString());
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            LstVwCD.Items.Clear();
            LstVwCdg.Items.Clear();
            LstVwCntnd.Items.Clear();
            LstVwDscpn.Items.Clear();
            LstVwLchl.Items.Clear();
            LstVwMpq.Items.Clear();
            LstVwMrc.Items.Clear();
            LstVwNcnldd.Items.Clear();
            LstVwNmbr.Items.Clear();
            LstVwPCIEPSIVA.Items.Clear();

            foreach (Cerveza cerveza in inventarios.Lista)
            {
                LstVwCD.Items.Add(cerveza.Cantidad.ToString());
                LstVwCdg.Items.Add(cerveza.CodigoSerial);
                LstVwCntnd.Items.Add(EContenido.getString(cerveza.CervezaCarac.Contenido));
                LstVwDscpn.Items.Add(cerveza.Descripcion);
                LstVwLchl.Items.Add(EGrado.getString(cerveza.CervezaCarac.Grado));
                LstVwMpq.Items.Add(EEmpaque.getString(cerveza.CervezaCarac.Empaque));
                LstVwMrc.Items.Add(ECerveceria.getString(cerveza.CervezaCarac.Cerveceria));
                LstVwNcnldd.Items.Add(EPais.getString(cerveza.CervezaCarac.Pais));
                LstVwNmbr.Items.Add(cerveza.Nombre);
                LstVwPCIEPSIVA.Items.Add(cerveza.PrecioVenta.ToString());
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Modificar modificar = new Modificar(inventarios);
            modificar.ShowDialog();
        }
    }
}
