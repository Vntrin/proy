﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Classes;

namespace GUI_Librado.Forms
{
    public partial class Códigos : Form
    {
        static InventarioC inventario = new InventarioC();
        public Códigos(InventarioC inventarios)
        {
            InitializeComponent();
            inventario = inventarios;
            foreach(Cerveza cerveza in inventario.Lista)
            {
                listView1.Items.Add(cerveza.ToString());
                listView2.Items.Add(cerveza.CodigoSerial);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Close();
                
        }
    }
}
