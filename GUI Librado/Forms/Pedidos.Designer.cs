﻿namespace GUI_Librado
{
    partial class Pedidos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LstVw = new System.Windows.Forms.ListView();
            this.LstVwDTE = new System.Windows.Forms.ListView();
            this.LstVwPrdct = new System.Windows.Forms.ListView();
            this.LstVwTp = new System.Windows.Forms.ListView();
            this.LstVwStd = new System.Windows.Forms.ListView();
            this.LstViwPrc = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Fecha";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(439, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 0;
            this.label2.Text = "Producto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(645, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 25);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tipo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(844, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 25);
            this.label4.TabIndex = 0;
            this.label4.Text = "Estado";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 6;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 227F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 199F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 192F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.LstVw, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwDTE, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwPrdct, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwTp, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstVwStd, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.LstViwPrc, 5, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(24, 23);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.380334F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94.61967F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1236, 539);
            this.tableLayoutPanel1.TabIndex = 1;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1050, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(127, 25);
            this.label5.TabIndex = 0;
            this.label5.Text = "Precio Total";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(209, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(153, 25);
            this.label6.TabIndex = 1;
            this.label6.Text = "Fecha Entrega";
            // 
            // LstVw
            // 
            this.LstVw.AutoArrange = false;
            this.LstVw.Location = new System.Drawing.Point(3, 31);
            this.LstVw.Name = "LstVw";
            this.LstVw.Size = new System.Drawing.Size(200, 505);
            this.LstVw.TabIndex = 2;
            this.LstVw.UseCompatibleStateImageBehavior = false;
            this.LstVw.View = System.Windows.Forms.View.List;
            // 
            // LstVwDTE
            // 
            this.LstVwDTE.Location = new System.Drawing.Point(209, 31);
            this.LstVwDTE.Name = "LstVwDTE";
            this.LstVwDTE.Size = new System.Drawing.Size(221, 505);
            this.LstVwDTE.TabIndex = 3;
            this.LstVwDTE.UseCompatibleStateImageBehavior = false;
            this.LstVwDTE.View = System.Windows.Forms.View.List;
            // 
            // LstVwPrdct
            // 
            this.LstVwPrdct.Location = new System.Drawing.Point(436, 31);
            this.LstVwPrdct.Name = "LstVwPrdct";
            this.LstVwPrdct.Size = new System.Drawing.Size(200, 505);
            this.LstVwPrdct.TabIndex = 4;
            this.LstVwPrdct.UseCompatibleStateImageBehavior = false;
            this.LstVwPrdct.View = System.Windows.Forms.View.List;
            // 
            // LstVwTp
            // 
            this.LstVwTp.Location = new System.Drawing.Point(642, 31);
            this.LstVwTp.Name = "LstVwTp";
            this.LstVwTp.Size = new System.Drawing.Size(193, 505);
            this.LstVwTp.TabIndex = 5;
            this.LstVwTp.UseCompatibleStateImageBehavior = false;
            this.LstVwTp.View = System.Windows.Forms.View.List;
            // 
            // LstVwStd
            // 
            this.LstVwStd.Location = new System.Drawing.Point(841, 31);
            this.LstVwStd.Name = "LstVwStd";
            this.LstVwStd.Size = new System.Drawing.Size(200, 505);
            this.LstVwStd.TabIndex = 6;
            this.LstVwStd.UseCompatibleStateImageBehavior = false;
            this.LstVwStd.View = System.Windows.Forms.View.List;
            // 
            // LstViwPrc
            // 
            this.LstViwPrc.Location = new System.Drawing.Point(1047, 31);
            this.LstViwPrc.Name = "LstViwPrc";
            this.LstViwPrc.Size = new System.Drawing.Size(186, 505);
            this.LstViwPrc.TabIndex = 7;
            this.LstViwPrc.UseCompatibleStateImageBehavior = false;
            this.LstViwPrc.View = System.Windows.Forms.View.List;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(36, 592);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(280, 44);
            this.button1.TabIndex = 2;
            this.button1.Text = "Añadir Pedido";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(328, 592);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(280, 44);
            this.button2.TabIndex = 2;
            this.button2.Text = "Añadir Reabastecimiento";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(1079, 592);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(154, 43);
            this.button3.TabIndex = 3;
            this.button3.Text = "Regresar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // Pedidos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 660);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Pedidos";
            this.Text = "Pedidos";
            this.Load += new System.EventHandler(this.Pedidos_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView LstVw;
        private System.Windows.Forms.ListView LstVwDTE;
        private System.Windows.Forms.ListView LstVwPrdct;
        private System.Windows.Forms.ListView LstVwTp;
        private System.Windows.Forms.ListView LstVwStd;
        private System.Windows.Forms.ListView LstViwPrc;
        private System.Windows.Forms.Button button3;
    }
}