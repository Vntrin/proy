﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Classes;

namespace GUI_Librado
{
    public partial class AñadirReabastecimiento : Form
    {
        static ArchivosPedidos archivosPedido = new ArchivosPedidos();
        static InventarioC inventario = new InventarioC();
        public AñadirReabastecimiento(ArchivosPedidos archivosPedidos, InventarioC inventarios)
        {
            InitializeComponent();
            archivosPedido = archivosPedidos;
            inventario = inventarios;
            foreach (Cerveza cerveza in inventario.Lista)
                checkedListBox1.Items.Add(cerveza.CodigoSerial);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PedidoCompra pedido = new PedidoCompra(dateTimePicker1.Value, textBox3.Text, inventario.BusquedaProductos(checkedListBox1.Text));
            pedido.CalcularPrecio();
            inventario.BusquedaProductos(checkedListBox1.SelectedItem.ToString()).Cantidad += (int)numericUpDown1.Value;
            archivosPedido.Compras.Add(pedido);
            Close();
        }
    }
}
