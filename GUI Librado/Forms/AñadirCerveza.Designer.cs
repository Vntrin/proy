﻿namespace GUI_Librado
{
    partial class AñadirCerveza
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.TxtBxNmbr = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtBxDscpn = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TxtBxCnt = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.CmbBxMnm = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.CmbBxMrc = new System.Windows.Forms.ComboBox();
            this.CmbBxCntnd = new System.Windows.Forms.ComboBox();
            this.CmbBxMpq = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.CmbBxNcnldd = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.TxtBxPrcVn = new System.Windows.Forms.TextBox();
            this.TxtBxPrcCm = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CmbBxLchl = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            // 
            // TxtBxNmbr
            // 
            this.TxtBxNmbr.Location = new System.Drawing.Point(306, 6);
            this.TxtBxNmbr.Margin = new System.Windows.Forms.Padding(6);
            this.TxtBxNmbr.Name = "TxtBxNmbr";
            this.TxtBxNmbr.Size = new System.Drawing.Size(292, 31);
            this.TxtBxNmbr.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 44);
            this.label2.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Marca";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 176);
            this.label4.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 25);
            this.label4.TabIndex = 2;
            this.label4.Text = "Empaque";
            // 
            // TxtBxDscpn
            // 
            this.TxtBxDscpn.Location = new System.Drawing.Point(306, 94);
            this.TxtBxDscpn.Margin = new System.Windows.Forms.Padding(6);
            this.TxtBxDscpn.Name = "TxtBxDscpn";
            this.TxtBxDscpn.Size = new System.Drawing.Size(632, 31);
            this.TxtBxDscpn.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 88);
            this.label3.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 25);
            this.label3.TabIndex = 2;
            this.label3.Text = "Descripción";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 132);
            this.label5.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(161, 25);
            this.label5.TabIndex = 2;
            this.label5.Text = "Contenido Neto";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(739, 572);
            this.button1.Margin = new System.Windows.Forms.Padding(6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 44);
            this.button1.TabIndex = 1;
            this.button1.Text = "Aceptar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(901, 572);
            this.button2.Margin = new System.Windows.Forms.Padding(6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 44);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancelar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(48, 554);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox1.Size = new System.Drawing.Size(400, 98);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Código(Autogenerado)";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(284, 37);
            this.button3.Margin = new System.Windows.Forms.Padding(6);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(104, 44);
            this.button3.TabIndex = 0;
            this.button3.Text = "Ayuda";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 46);
            this.label6.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 25);
            this.label6.TabIndex = 2;
            this.label6.Text = "Placeholder";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.46955F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.53045F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.TxtBxCnt, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxMnm, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label12, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.TxtBxNmbr, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TxtBxDscpn, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxMrc, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxCntnd, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxMpq, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxNcnldd, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.label11, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.TxtBxPrcVn, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.TxtBxPrcCm, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.CmbBxLchl, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 7);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(24, 23);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(6);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1018, 474);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // TxtBxCnt
            // 
            this.TxtBxCnt.Location = new System.Drawing.Point(306, 226);
            this.TxtBxCnt.Margin = new System.Windows.Forms.Padding(6);
            this.TxtBxCnt.Name = "TxtBxCnt";
            this.TxtBxCnt.Size = new System.Drawing.Size(154, 31);
            this.TxtBxCnt.TabIndex = 1;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 220);
            this.label9.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 25);
            this.label9.TabIndex = 2;
            this.label9.Text = "Cantidad";
            // 
            // CmbBxMnm
            // 
            this.CmbBxMnm.FormattingEnabled = true;
            this.CmbBxMnm.Location = new System.Drawing.Point(303, 267);
            this.CmbBxMnm.Name = "CmbBxMnm";
            this.CmbBxMnm.Size = new System.Drawing.Size(121, 33);
            this.CmbBxMnm.TabIndex = 12;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 264);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(238, 25);
            this.label12.TabIndex = 11;
            this.label12.Text = "Minimo que debe haber";
            // 
            // CmbBxMrc
            // 
            this.CmbBxMrc.FormattingEnabled = true;
            this.CmbBxMrc.Location = new System.Drawing.Point(303, 47);
            this.CmbBxMrc.Name = "CmbBxMrc";
            this.CmbBxMrc.Size = new System.Drawing.Size(121, 33);
            this.CmbBxMrc.TabIndex = 3;
            // 
            // CmbBxCntnd
            // 
            this.CmbBxCntnd.FormattingEnabled = true;
            this.CmbBxCntnd.Location = new System.Drawing.Point(303, 135);
            this.CmbBxCntnd.Name = "CmbBxCntnd";
            this.CmbBxCntnd.Size = new System.Drawing.Size(121, 33);
            this.CmbBxCntnd.TabIndex = 4;
            // 
            // CmbBxMpq
            // 
            this.CmbBxMpq.FormattingEnabled = true;
            this.CmbBxMpq.Location = new System.Drawing.Point(303, 179);
            this.CmbBxMpq.Name = "CmbBxMpq";
            this.CmbBxMpq.Size = new System.Drawing.Size(121, 33);
            this.CmbBxMpq.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 436);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(137, 25);
            this.label10.TabIndex = 7;
            this.label10.Text = "Nacionalidad";
            // 
            // CmbBxNcnldd
            // 
            this.CmbBxNcnldd.FormattingEnabled = true;
            this.CmbBxNcnldd.Location = new System.Drawing.Point(303, 439);
            this.CmbBxNcnldd.Name = "CmbBxNcnldd";
            this.CmbBxNcnldd.Size = new System.Drawing.Size(121, 33);
            this.CmbBxNcnldd.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 397);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(135, 25);
            this.label11.TabIndex = 10;
            this.label11.Text = "Precio Venta";
            // 
            // TxtBxPrcVn
            // 
            this.TxtBxPrcVn.Location = new System.Drawing.Point(303, 400);
            this.TxtBxPrcVn.Name = "TxtBxPrcVn";
            this.TxtBxPrcVn.Size = new System.Drawing.Size(157, 31);
            this.TxtBxPrcVn.TabIndex = 9;
            // 
            // TxtBxPrcCm
            // 
            this.TxtBxPrcCm.Location = new System.Drawing.Point(306, 358);
            this.TxtBxPrcCm.Margin = new System.Windows.Forms.Padding(6);
            this.TxtBxPrcCm.Name = "TxtBxPrcCm";
            this.TxtBxPrcCm.Size = new System.Drawing.Size(154, 31);
            this.TxtBxPrcCm.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 352);
            this.label8.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 25);
            this.label8.TabIndex = 2;
            this.label8.Text = "Precio Compra";
            // 
            // CmbBxLchl
            // 
            this.CmbBxLchl.FormattingEnabled = true;
            this.CmbBxLchl.Location = new System.Drawing.Point(303, 311);
            this.CmbBxLchl.Name = "CmbBxLchl";
            this.CmbBxLchl.Size = new System.Drawing.Size(121, 33);
            this.CmbBxLchl.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 308);
            this.label7.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(178, 25);
            this.label7.TabIndex = 2;
            this.label7.Text = "Grado de Alcohol";
            // 
            // AñadirCerveza
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 706);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "AñadirCerveza";
            this.Text = "AñadirCerveza";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TxtBxNmbr;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtBxDscpn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox TxtBxPrcCm;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TxtBxCnt;
        private System.Windows.Forms.ComboBox CmbBxMrc;
        private System.Windows.Forms.ComboBox CmbBxCntnd;
        private System.Windows.Forms.ComboBox CmbBxMpq;
        private System.Windows.Forms.ComboBox CmbBxLchl;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox CmbBxNcnldd;
        private System.Windows.Forms.TextBox TxtBxPrcVn;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CmbBxMnm;
        private System.Windows.Forms.Label label12;
    }
}