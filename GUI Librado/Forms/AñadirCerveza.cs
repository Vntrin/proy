﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Forms;
using GUI_Librado.Classes;
using GUI_Librado.Enums;

namespace GUI_Librado
{
    public partial class AñadirCerveza : Form
    {
        static InventarioC inventarios = new InventarioC();

        public AñadirCerveza(InventarioC inventario)
        {
            InitializeComponent();
            inventarios = inventario;
            CmbBxCntnd.DataSource = Enum.GetValues(typeof(Contenido));
            CmbBxLchl.DataSource = Enum.GetValues(typeof(Grado));
            CmbBxMpq.DataSource = Enum.GetValues(typeof(Empaque));
            CmbBxMrc.DataSource = Enum.GetValues(typeof(Cerveceria));
            CmbBxNcnldd.DataSource = Enum.GetValues(typeof(Pais));
            CmbBxMnm.DataSource = Enum.GetValues(typeof(Minimo));
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Ayuda ayuda = new Ayuda();
            ayuda.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cerveza cerveza= new Cerveza(TxtBxNmbr.Text, TxtBxDscpn.Text,
                new CervezaCarac((Cerveceria)CmbBxMrc.SelectedItem, (Contenido)CmbBxCntnd.SelectedItem,
                (Empaque)CmbBxMpq.SelectedItem, (Grado)CmbBxLchl.SelectedItem, (Pais)CmbBxNcnldd.SelectedItem, (Minimo)CmbBxMnm.SelectedItem),
                Int32.Parse(TxtBxCnt.Text), Double.Parse(TxtBxPrcCm.Text), Double.Parse(TxtBxPrcVn.Text));
            cerveza.GenerarCodigo();
            inventarios.DarAltaProducto(cerveza);
            Close();
        }
    }
}
