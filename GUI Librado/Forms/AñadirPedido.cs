﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GUI_Librado.Classes;

namespace GUI_Librado
{
    public partial class AñadirPedido : Form
    {
        static ArchivosPedidos archivosPedido = new ArchivosPedidos();
        static InventarioC inventario = new InventarioC();
        public AñadirPedido(ArchivosPedidos archivosPedidos, InventarioC inventarios)
        {
            InitializeComponent();
            archivosPedido = archivosPedidos;
            inventario = inventarios;
            foreach (Cerveza cerveza in inventario.Lista)
                checkedListBox1.Items.Add( cerveza.CodigoSerial);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PedidoVenta pedido = new PedidoVenta(DtTmP.Value, TxtBxClnt.Text, inventario.BusquedaProductos(checkedListBox1.SelectedItem.ToString()),
                false, DtTmE.Value);
            pedido.CalcularPrecio();
            inventario.BusquedaProductos(checkedListBox1.SelectedItem.ToString()).Cantidad -= (int)NmrcUpDwnCC.Value;
            archivosPedido.Ventas.Add(pedido);
            Close();
        }
    }
}
