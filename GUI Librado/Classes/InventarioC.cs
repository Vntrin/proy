﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI_Librado.Enums;
using GUI_Librado.Classes;

namespace GUI_Librado.Classes
{
    public class InventarioC
    {
        List<Cerveza> lista;

        public InventarioC()
        {
            lista = new List<Cerveza>();
            lista.Add(new Cerveza("Reina Oscura", "Cerveza Oscura Lager",new CervezaCarac(Cerveceria.CerReina, Contenido.ml355, Empaque.C24,
                Grado.D0A4, Pais.Mexico, Minimo.Min5), 10, 1.9, 2.8));
            lista.Add(new Cerveza("Reina Clara", "Cerveza Clara Lager", new CervezaCarac(Cerveceria.CerReina, Contenido.ml355, Empaque.C24,
                Grado.D0A4, Pais.Mexico, Minimo.Min5), 30, 1.9, 2.8));
            lista.Add(new Cerveza("Hefe Weissbier Naturtrub", "Cerveza de trigo turbia clara", new CervezaCarac(Cerveceria.CerPaulander, Contenido.ml500, Empaque.C20,
                Grado.D4A6, Pais.Alemania, Minimo.Min2), 20, 1.9, 2.8));
            lista.Add(new Cerveza("Hefe Weissbier Dunkel", "Cerveza de trigo turbia obscura", new CervezaCarac(Cerveceria.CerPaulander, Contenido.ml500, Empaque.C20,
                Grado.D4A6, Pais.Alemania, Minimo.Min4), 15, 1.9, 2.8));
            lista.Add(new Cerveza("Bernard Bohemian", "Cerveza Clara Ale", new CervezaCarac(Cerveceria.CerBernard, Contenido.ml330, Empaque.C24,
                Grado.D6A8, Pais.RepCheca, Minimo.Min2), 21, 1.9, 2.8));
            lista.Add(new Cerveza("Bernard Clara", "Cerveza Clara Lager", new CervezaCarac(Cerveceria.CerBernard, Contenido.ml500, Empaque.C20,
                Grado.D4A6, Pais.RepCheca, Minimo.Min5), 43, 1.9, 2.8));
            lista.Add(new Cerveza("Moritz", "Cerveza Lager", new CervezaCarac(Cerveceria.CerMoritz, Contenido.ml330, Empaque.C24,
                Grado.D4A6, Pais.Espana, Minimo.Min2), 45, 1.9, 2.8));
            lista.Add(new Cerveza("Moritz epidor", "Cerveza Lager Ambar", new CervezaCarac(Cerveceria.CerMoritz, Contenido.ml330, Empaque.C24,
                Grado.D6A8, Pais.Espana, Minimo.Min4), 34, 1.9, 2.8));
            foreach (Cerveza cerve in lista)
            {
                cerve.CodigoSerial = cerve.GenerarCodigo();
            }
        }

        public void DarAltaProducto(Cerveza cerveza)
        {
            lista.Add(cerveza);
        }

        public void AgregarInventario(PedidoCompra pedido)
        {
            foreach(Cerveza cerve in lista)
            {
                if (cerve.Compare(pedido.Cervezas.CodigoSerial))
                    pedido.Cervezas.Cantidad += cerve.Cantidad;
            }
        }

        public List<Cerveza> FormulaReabastecimiento()
        {
            List<Cerveza> cervezaPorAgotarse = new List<Cerveza>();
            foreach(Cerveza cerveza in lista)
            {
                if ((int)cerveza.CervezaCarac.Minimo == cerveza.Cantidad)
                    cervezaPorAgotarse.Add(cerveza);
            }
            return cervezaPorAgotarse;
        }

        public void QuitarDeInventario(PedidoVenta pedido)
        {
            foreach (Cerveza cerve in lista)
            {
                if (cerve.Compare(pedido.Cervezas.CodigoSerial))
                    pedido.Cervezas.Cantidad -= cerve.Cantidad;
            }
        }

        public void CambiosImprevistos(Cerveza cerveza)
        {
            foreach (Cerveza cerv in lista)
            {
                if (cerv.Compare(cerveza.CodigoSerial))
                {
                    cerv.QuitarCantidad(cerveza);
                }
            }
        }

        public Cerveza BusquedaProductos(string cervezas)
        {
        
            foreach (Cerveza cerveza in lista)
            {
                if (cerveza.Compare(cervezas))
                        return cerveza;
                
            }
            return null;
        }

        public List<Cerveza> Lista { get => lista; set => lista = value; }
    }
}
