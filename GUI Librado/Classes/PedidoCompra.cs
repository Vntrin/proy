﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Librado.Classes
{
    public class PedidoCompra:Pedido
    {
        public PedidoCompra(DateTime fecha, string mercader, Cerveza cervezas) : base(fecha, mercader, cervezas)
        {
        }

        public override void CalcularPrecio()
        {
            Precio += (Cervezas.Cantidad * Cervezas.PrecioCompra);
            Precio=-Precio;
        }
    }
}
