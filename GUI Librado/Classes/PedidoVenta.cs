﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_Librado.Classes
{
    public class PedidoVenta:Pedido
    {
        bool estado;
        DateTime fechaEntrega;

        public PedidoVenta(DateTime fechaOrden, string mercader, Cerveza cervezas,
            bool estado, DateTime fechaEntrega) : base(fechaOrden, mercader, cervezas)
        {
            this.estado = estado;
            this.fechaEntrega = fechaEntrega;
        }

        public DateTime FechaEntrega { get => fechaEntrega; set => fechaEntrega = value; }
        public bool Estado { get => estado; set => estado = value; }

        public override void CalcularPrecio()
        {
            Precio += (Cervezas.Cantidad * Cervezas.PrecioVenta);            
        }

    }
}
