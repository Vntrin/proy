﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI_Librado.Enums;

namespace GUI_Librado.Classes
{
    public class Cerveza
    {
        string nombre;
        string codigoSerial;
        string descripcion;
        CervezaCarac cervezaCarac;
        int cantidad;
        double precioCompra;
        double precioVenta;

        public Cerveza(string nombre, string descripcion,CervezaCarac cervezaCarac, int cantidad, 
            double precioCompra, double precioVenta)
        {
            this.nombre = nombre;
            this.descripcion = descripcion;
            this.cervezaCarac = cervezaCarac;
            this.cantidad = cantidad;
            this.precioCompra = precioCompra;
            this.precioVenta = precioVenta;
            codigoSerial = null;
        }

        public bool Compare(string codigo)
        {
            if (codigoSerial == codigo)
                return true;
            return false;
        }

        public void AgregarCantidad(Cerveza cerv)
        {
            cantidad += cerv.Cantidad;
        }

        public void QuitarCantidad(Cerveza cerv)
        {
            cantidad -= cerv.Cantidad;
        }

        public string GenerarCodigo()
        {
            char[] codigo= {' ',' ',' ',' ',' ',' ',' ' };
            string[] nSep;

            nSep = nombre.Split(' ');
            int i = 0;
            for (i= 0; i < nSep.Length; i++)
            {
                codigo[i]= nSep[i].First();
            }
            if (cervezaCarac.Empaque == Empaque.C20)
            {
                codigo[i++] = 'C';
                codigo[i++] = '2';
                codigo[i++] = '0';
            }
            if (cervezaCarac.Empaque == Empaque.C24)
            {
                codigo[i++] = 'C';
                codigo[i++] = '2';
                codigo[i++] = '4';
            }
            if (cervezaCarac.Empaque == Empaque.Pieza)
            {
                codigo[i++] = 'P';
            }
            codigoSerial = new string(codigo).ToUpper();

            return codigoSerial;
        }

        public override string ToString()
        {
            return nombre + " " +CervezaCarac.Cerveceria.ToString() + " " + cervezaCarac.Pais.ToString();
        }

        public string Nombre { get => nombre; set => nombre = value; }
        public string CodigoSerial { get => codigoSerial; set => codigoSerial = value; }
        public CervezaCarac CervezaCarac { get => cervezaCarac; set => cervezaCarac = value; }
        public int Cantidad { get => cantidad; set => cantidad = value; }
        public double PrecioCompra { get => precioCompra; set => precioCompra = value; }
        public double PrecioVenta { get => precioVenta; set => precioVenta = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
    }
}
