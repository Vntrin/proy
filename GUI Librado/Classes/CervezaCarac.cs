﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI_Librado.Enums;

namespace GUI_Librado.Classes
{
    public class CervezaCarac
    {
        Cerveceria cerveceria;
        Contenido contenido;
        Empaque empaque;
        Grado grado;
        Pais pais;
        Minimo minimo;

        public CervezaCarac(Cerveceria cerveceria, Contenido contenido, Empaque empaque,
            Grado grado, Pais pais, Minimo minimo)
        {
            this.cerveceria = cerveceria;
            this.contenido = contenido;
            this.empaque = empaque;
            this.grado = grado;
            this.pais = pais;
            this.minimo = minimo;
        }

        public Cerveceria Cerveceria { get => cerveceria; set => cerveceria = value; }
        public Contenido Contenido { get => contenido; set => contenido = value; }
        public Empaque Empaque { get => empaque; set => empaque = value; }
        public Grado Grado { get => grado; set => grado = value; }
        public Pais Pais { get => pais; set => pais = value; }
        public Minimo Minimo { get => minimo; set => minimo = value; }
    }
}
