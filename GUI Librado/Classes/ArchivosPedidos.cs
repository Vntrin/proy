﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GUI_Librado.Enums;

namespace GUI_Librado.Classes
{
    public class ArchivosPedidos
    {
        List<PedidoCompra> compras;
        List<PedidoVenta> ventas;
        List<PedidoVenta> inconclusos;

        public ArchivosPedidos()
        {
            compras = new List<PedidoCompra>();
            ventas = new List<PedidoVenta>();
            inconclusos = new List<PedidoVenta>();
            compras.Add(new PedidoCompra(new DateTime(2008, 5, 1, 8, 30, 52), "sdad",
                new Cerveza("Reina Clara", "afdasfasf", new CervezaCarac(Cerveceria.CerBernard, Contenido.L5, Empaque.C20,
                Grado.D0A4, Pais.Alemania, Minimo.Min2), 10, 1.9, 2.8)));
        }

        public List<PedidoCompra> Compras { get => compras; set => compras = value; }
        public List<PedidoVenta> Ventas { get => ventas; set => ventas = value; }
        public List<PedidoVenta> Inconclusos { get => inconclusos; set => inconclusos = value; }
    }
}
